package id.ub.papb.calculator2_205150401111048;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        hasil = findViewById(R.id.hasil);
        Intent intent = getIntent();
        int hasil1 = intent.getIntExtra("hasil",0);
        String hasil2 = String.valueOf(hasil1);
        hasil.setText(intent.getStringExtra("operasi")+ " "+ hasil2);
    }
}
